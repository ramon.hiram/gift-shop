<?php

namespace App\Http\Controllers;

use App\Http\Resources\CartResource;
use App\Cart;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user())
        {
            $cart = Cart::where('user_id', auth()->user()->id)->with('product')->get();
            return response()->json(CartResource::collection($cart));
        }
        else
        {
            return response()->json(null);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Add a product 
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $product_id = $request->get('product');

        if (auth()->user())
        { 
            $cart = Cart::create([
                'product_id' => $product_id, 
                'user_id' => auth()->user()->id
                ]);   
        }

        if($cart)
        return response()->json(['success'=> true]);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function remove(Cart $cart)
    {
        $cart->delete();

        return response()->json(['success' => true]);
    }
}
