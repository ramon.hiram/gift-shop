<?php

namespace App\Http\Controllers;

use App\{Product, Media};
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        
        return response()->json(ProductResource::collection($products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $request->validated();

        try
        {
            $product = Product::create($request->except('image'));
    
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $product->addMediaFromRequest('image')->toMediaCollection('images');
            }
            return response()->json(['success' => true]);
        }
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'message' => 'Hubo un error al guardar los datos del producto, intente nuevamente o contacte al administrador', 'errors' => $e]);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->json(new ProductResource($product));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id) //Route Binding isn't working
    {
        $request->validated();
        $product = Product::find($id);
        try
        {
            $product->update($request->except('image'));
            $product->save();

            if($request->hasFile('image') && $request->file('image')->isValid()){
                $media = $product->getFirstMedia('images');
                $media->delete();
                $product->addMediaFromRequest('image')->preservingOriginal()->toMediaCollection('images');
            }
            return response()->json(['success' => true]);
        }
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'message' => 'Hubo un error al guardar los datos del producto, intente nuevamente o contacte al administrador', 'errors' => $e]);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product Pending: It doesn't work with Route Binding
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $product = Product::find($id);
        $this->authorize('delete',$product);

        $product->delete();
        
        return response()->json(['success' => true]);

    }
    public function showImage($id)
    {   
        $product = Product::find($id);
        $media = $product->getFirstMedia('images');
           
        return response()->download($media->getPath('thumb'), $media->file_name, [], 'inline');
    }
}
