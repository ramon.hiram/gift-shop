<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = $this->whenLoaded('product');
        return [
            'id' => $this->id,
            'name' => $product->name,
            'price' =>$product->price,
            'description' =>$product->description,
            'image' => $this->when($product->hasMedia('images'), 
                URL::temporarySignedRoute('product.thumb', now()->addDays(1),['product' => $product])),
        ];
    }
}
