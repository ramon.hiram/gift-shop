<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    /**
     * Determine whether the user can create products.
     *
     
     */
    public function create(User $user)
    {
        return $user->can('create products');
    }

    /**
     * Determine whether the user can delete products.
     *
     */
    public function delete(User $user, Product $product)
    {
        return $user->can('delete products');
    }

    /**
     * Determine whether the user can update products.
     *
     */
    public function update(User $user)
    {
        return $user->can('update products');
    }
}
