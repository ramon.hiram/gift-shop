<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    
    
    
    
    
    // scopes 

    public function scopeGeneratePurchaseNumber($query)
    {
        $existing_order = $query->latest()->value('purchase_order_number');
        
        if($existing_order)
        {
            $purchase_order_number = sprintf('%011d', $existing_order + 1 );
        }
        else
        {
            $purchase_order_number = '000000000001';
        }
        return $purchase_order_number;
    }    
    
    
    
    // Relationships

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
