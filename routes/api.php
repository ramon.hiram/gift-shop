<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Auth Routes


Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

// Product Routes
Route::prefix('products')->group(function () {

    Route::get('/', 'ProductController@index');
    Route::get('/show/{product}', 'ProductController@show');
    
    Route::middleware('auth:api')->group(function () {
        Route::post('/', 'ProductController@store');
        Route::get('/{slug}/edit', 'ProductController@show');
        Route::put('/update/{product}', 'ProductController@update');
        Route::delete('delete/{product}', 'ProductController@destroy');
    });
    
});

// Shopping Cart Routes
Route::prefix('cart')->group(function () {

    Route::prefix('products')->group(function () {
        Route::get('/', 'CartController@index');
        Route::post('add', 'CartController@add');
        Route::delete('remove/{cart}', 'CartController@remove');
        // Route::get('/show/{product}', 'CartController@show');
        
        Route::middleware(['permission:buy products'])->group(function() {
            Route::post('buy', 'PurchaseController@store');
        });        
    });
    
});



Route::prefix('/thumbs')->middleware('signed')->group(function () {
    Route::get('products/{product}', 'ProductController@showImage')->name('product.thumb');
});
