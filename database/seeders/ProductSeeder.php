<?php

namespace Database\Seeders;

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(['name' => 'Tarjeta de cumpleaños con globos', 'description'=>'(20x15cm) sin doblez, fondo negro y gris con decoración de globos', 'price'=>80, 'category_id'=>1])->addMedia('storage/app/public/products/1.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Tarjeta de cumpleaños de colores', 'description'=>'(15x10cm) fondo blanco, sin doblez con mensaje en la parte superior derecha, decoración de colores', 'price'=>75, 'category_id'=>1])->addMedia('storage/app/public/products/2.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Tarjeta de San Valentin', 'description'=>'Fondo negro con doblez, inscripcion en portada y decoración con corazones', 'price'=>95, 'category_id'=>1])->addMedia('storage/app/public/products/3.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Globos para decoración metálicos', 'description'=>'Colores varios Medianos, 23 piezas', 'price'=>324.90, 'category_id'=>2])->addMedia('storage/app/public/products/4.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Globos de decoración en letras', 'description'=>'Color amarillo 13 piezas', 'price'=>250, 'category_id'=>2])->addMedia('storage/app/public/products/5.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Taza día de las madres', 'description'=>'Fondo blanco con inscripción (400 ml)', 'price'=>180, 'category_id'=>4])->addMedia('storage/app/public/products/6.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Taza Rick y Morty', 'description'=>'Fondo blanco y cara de Rick', 'price'=>300, 'category_id'=>4])->addMedia('storage/app/public/products/7.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Cojín LOVE con relleno', 'description'=>'Funda blanca con la palabra escrita (LOVE)', 'price'=>270, 'category_id'=>5])->addMedia('storage/app/public/products/8.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Cojín cumpleaños', 'description'=>'(50 x 50cm) con inscripción, relleno, funda color blanco', 'price'=>235, 'category_id'=>5])->addMedia('storage/app/public/products/9.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Oso de peluche blanco', 'description'=>'(90 cm alto y 50 cm ancho) corazón en pecho con inscripción', 'price'=>799, 'category_id'=>3])->addMedia('storage/app/public/products/10.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Pareja de peluches oso pardo y conejo blanco', 'description'=>'Tamaño mediano', 'price'=>589, 'category_id'=>3])->addMedia('storage/app/public/products/11.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Marco para papá de madera', 'description'=>'(10x18 cm) con inscripción en parte superior e inferior', 'price'=>189.90, 'category_id'=>2])->addMedia('storage/app/public/products/12.png')->preservingOriginal()->toMediaCollection('images');
        Product::create(['name' => 'Porta retrato abuelos de madera', 'description'=>'50 cm, con mensaje inscrito', 'price'=>150, 'category_id'=>2])->addMedia('storage/app/public/products/13.png')->preservingOriginal()->toMediaCollection('images');
    }
}
