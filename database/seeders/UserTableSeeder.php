<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;



class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        User::firstOrCreate([
            'username' => 'admin',
            'password' => Hash::make('password'),
        ])->assignRole('admin');

        User::firstOrCreate([
            'username' => 'user',
            'password' => Hash::make('password'),
        ])->assignRole('user');



    }
}
