<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\{Permission, Role};

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permission
        Permission::firstOrCreate(['name' => 'create products']);
        Permission::firstOrCreate(['name' => 'update products']);
        Permission::firstOrCreate(['name' => 'delete products']);
        Permission::firstOrCreate(['name' => 'list products']);
        Permission::firstOrCreate(['name' => 'add products']);
        Permission::firstOrCreate(['name' => 'buy products']);
        


        //Roles
        $admin = Role::firstOrCreate(['name' => 'admin']);
        $user = Role::firstOrCreate(['name' => 'user']);
        $guest = Role::firstOrCreate(['name' => 'guest']);

        //Permissions Asignement
        $admin->givePermissionTo(Permission::all());

        $user->givePermissionTo(
            [
                'list products',
                'add products',
                'buy products',
            ]
        );

        $guest->givePermissionTo(
            [
                'list products',
                'add products',
            ]
        );

    }
}
