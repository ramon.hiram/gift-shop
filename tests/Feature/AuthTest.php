<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\{RefreshDatabase,WithFaker};
use Tests\TestCase;
use App\User;
use Laravel\Passport\Passport;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
     /** @test */
    public function login_as_admin()
    {
        // prepare
        $user = User::factory()->create([
            'username' => 'admin',
        ])->assignRole('admin');

        // act
        $response = $this->json('POST', 'api/auth/login', ['username' => 'admin','password' => 'password']);

        // confirm
        $response->assertOk()->assertJsonStructure(
            ['access_token','token_type', 'expires_at' ]
        );
    }

    /** @test */
    public function login_as_registered_user()
    {
        // prepare
        $user = User::factory()->create([
            'username' => 'user',
        ])->assignRole('user');

        // act
        $response = $this->json('POST', 'api/auth/login', ['username' => 'user','password' => 'password']);

        //confirm
        $response->assertOk()
                ->assertJsonStructure(
                    ['access_token','token_type', 'expires_at' ]
                );
    }
     /** @test */
     public function logout_as_admin()
     {
         $this->handleValidationExceptions();
        // prepare
         $user = User::factory()->create([
             'username' => 'admin',
         ])->assignRole('admin');
 
        $admin = Passport::actingAs($user);     
        // act and confirm
        $this->actingAs($admin)->json('GET', 'api/auth/logout')
        ->assertOk()
        ->assertJson(['message' => 'Successfully logged out']);;
     }

     /** @test */
     public function logout_as_user()
     {
         $this->handleValidationExceptions();
 
         $user = User::factory()->create([
             'username' => 'user',
         ])->assignRole('user');
 
        $registered_user = Passport::actingAs($user);     

        $this->actingAs($registered_user)->json('GET', 'api/auth/logout')
        ->assertOk()
        ->assertJson(['message' => 'Successfully logged out']);
     }
}
