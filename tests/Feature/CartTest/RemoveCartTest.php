<?php

namespace Tests\Feature\CartTest;

use App\{User, Product, Cart};
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\TestCase;

class RemoveCartTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();  

        $this->products = Product::factory(3)->create()->each(function ($product) 
        {
            $image = UploadedFile::fake()->image("product.jpg");
            $product->addMedia($image)->toMediaCollection('images');
        });
    }

    
    /** @test */
    public function remove_product_from_shopping_cart()
    {
        $this->handleValidationExceptions();
        // prepare
        /* User */
        $this->user->assignRole('user');
        $user = Passport::actingAs($this->user);

        $this->products->each(function ($product) 
        {
            Cart::factory(['product_id' => $product->id, 'user_id' => $this->user->id])->create();
        });

        $cart = $this->user->cart->random(1);
        $cart_id = $cart->pluck('id');
        // act
        $response = $this->actingAs($user)->json('DELETE', "api/cart/products/remove/{$cart[0]}" );

        $this->assertDatabaseMissing('carts', ['id' => $cart[0]]);

    }
}
