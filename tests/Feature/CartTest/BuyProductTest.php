<?php

namespace Tests\Feature\CartTest;

use App\{User, Product, Cart};
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BuyProductTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();  

        $this->products = Product::factory(3)->create()->each(function ($product) 
        {
            $image = UploadedFile::fake()->image("product.jpg");
            $product->addMedia($image)->toMediaCollection('images');
        });
    }

    /** @test */
    public function buy_products()
    {
        $this->handleValidationExceptions();
        // prepare
        /* User */
        $this->user->assignRole('user');
        $user = Passport::actingAs($this->user);

        $this->products->each(function ($product) 
        {
            Cart::factory(['product_id' => $product->id, 'user_id' => $this->user->id])->create();
        });

        $data = [
            'products' => $this->user->cart
        ];
        
        // act
        $response = $this->actingAs($user)->json('POST', "api/cart/products/buy", $data);

        $response->assertJson(['success'=>true]);
        $this->assertDatabaseHas('purchases', ['user_id' => $this->user->id]);

    }
}
