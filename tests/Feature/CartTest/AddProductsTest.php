<?php

namespace Tests\Feature\CartTest;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AddProductsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();  

        $this->products = Product::factory(3)->create();
        
        $this->products->each(function ($product) 
        {
            $image = UploadedFile::fake()->image("product.jpg");
            $product->addMedia($image)->toMediaCollection('images');
        });
    }

    /** @test */
    public function add_product_to_shopping_cart()
    {
        $this->handleValidationExceptions();
        // prepare
        /* User */
        $this->user->assignRole('user');
        $user = Passport::actingAs($this->user);

        $product =  $this->products->random(1);
        
        // act
        $response = $this->actingAs($user)->json('POST', 'api/cart/products/add', ['product' => $product->pluck('id')[0]] );

        // confirm
        $response->assertJson(['success' => true]);

        // verify
        $this->assertDatabaseHas('carts',['user_id' => $this->user->id, 'product_id' => $product->pluck('id')[0], ]);        
    }
}
