<?php

namespace Tests\Feature\ProductTest;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Product;
use App\User;
use Laravel\Passport\Passport;

class UpdateProductTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();     
    }

    /** @test */
    public function update_product()
    {
        $this->handleValidationExceptions();
        // prepare
        
        $this->user->assignRole('admin');
        $admin = Passport::actingAs($this->user);

        // data 1
        $image = UploadedFile::fake()->image("product.jpg");
        $product = Product::factory()->create()->addMedia($image)->toMediaCollection('images');
        
        // data 2 (the new data)
        $image2 = UploadedFile::fake()->image("product2.jpg");
        $new_data = Product::factory()->make(['image' => $image2]);
        // act
        $response = $this->actingAs($admin)->json('PUT', "api/products/update/{$product->id}", $new_data->toArray());

        // confirm
        $response->assertJson(['success' => true]);

        $collection = collect($new_data)->except(['image', 'price']);
        
        // verify
        $this->assertDatabaseHas('products', $collection->toArray());
    }

}
