<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Product;
use App\User;
use Laravel\Passport\Passport;

class DeleteProductTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();     
    }
     /** @test */
    public function delete_product_as_admin()
    {
        $this->handleValidationExceptions();
        // prepare
        
        $this->user->assignRole('admin');
        $admin = Passport::actingAs($this->user);

        $product = Product::factory()->create();
        $image = UploadedFile::fake()->image("product.jpg");
        $product->addMedia($image)->toMediaCollection('images');
        
        // act
        $response = $this->actingAs($admin)->json('DELETE', "api/products/delete/{$product->id}");

        // confirm
        $response->assertJson(['success' => true]);
        
        // verify
        $this->assertSoftDeleted('products', ['id'=> $product->id, 'slug'=> $product->slug]);
    }

    /** @test */
    public function delete_product_as_user()
    {
        // prepare
        $this->user->assignRole('user');
        $user = Passport::actingAs($this->user);

        $product = Product::factory()->create();
        $image = UploadedFile::fake()->image("product.jpg");
        $product->addMedia($image)->toMediaCollection('images');
        
        // act and verify
        $response = $this->actingAs($user)->json('DELETE', "api/products/delete/{$product->id}")->assertForbidden(); // This sends a 403 status

    }
}
