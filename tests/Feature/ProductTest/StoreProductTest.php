<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Product;
use App\User;
use Laravel\Passport\Passport;


class StoreProductTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();     
    }

    /** @test */
    public function store_product()
    {
        $this->handleValidationExceptions();
        // prepare
        $this->user->assignRole('admin');
        $admin = Passport::actingAs($this->user);

        $product = Product::factory()->make(['image' => UploadedFile::fake()->image("product.jpg")]);
        
        // act
        $response = $this->actingAs($admin)->json('POST', 'api/products', $product->toArray());

        // confirm
        $response->assertJson(['success' => true]);

        $collection = collect($product)->except(['image', 'price']);
        
        // verify
        $this->assertDatabaseHas('products', $collection->toArray());
    }

}
