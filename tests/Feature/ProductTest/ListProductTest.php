<?php

namespace Tests\Feature;

use App\Http\Resources\ProductResource;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Product;
use App\User;
use Laravel\Passport\Passport;


class ListProductTest extends TestCase
{
    use RefreshDatabase;
    
     /** @test */
    public function list_products()
    {
        $this->handleValidationExceptions();
        
        // prepare
        $products = Product::factory(20)->create();
        foreach($products as $product){
            $image = UploadedFile::fake()->image("product.jpg");
            $product->addMedia($image)->toMediaCollection('images');
        }

        // act
        $response = $this->json('get', 'api/products');
        // verify
        $response->assertStatus(200);
    }
}
